import {derived, writable} from "svelte/store";

// We check in the URL if there is some template information, and gather it.
const urlSearchParams = new URLSearchParams(window.location.search);
const urlTemplate = {
  title: urlSearchParams.get("title"),
  inputText: urlSearchParams.get("inputText"),
  inputTextIsHtml: urlSearchParams.get("inputTextIsHtml") === "true",
};

// The view mode can be either "edit" (edit the model), "preview" (view the model), "fill" (fill the blanks to create our own model)
// If a template is already given via URL, turn off the "edit" mode and activate the "preview" mode directly
export const viewMode = writable(!urlTemplate.title || !urlTemplate.inputText ? "edit" : "preview");

// App inputs
export const title = writable(urlTemplate.title || ""); // Title of the model
export const inputText = writable(urlTemplate.inputText || ""); // The template to fill with data, either empty, or set to the input given in the url
export const inputTextIsHtml = writable(urlTemplate.inputTextIsHtml); // The user can tell if the model is either plain text or HTML

export const pageTitle = derived(
  [title, viewMode],
  ([title, viewMode]) =>
    viewMode === "edit"
      ? title
        ? `Editer le modèle "${title}"` // Editing mode + existing title
        : "Créer un nouveau modèle" // Editing mode + no title
      : title
      ? `Modèle "${title}"` // Preview mode + existing title
      : "" // Preview mode + no title at all
);

// Toasts states
export const sharingLinkCopiedToast = writable(false); // Displays the success notif toast when we copy the sharing link
export const contentCopiedToast = writable(false); // Displays the success notif toast when we copy the outputs
export const closeToast = () => {
  sharingLinkCopiedToast.set(false);
  contentCopiedToast.set(false);
};
