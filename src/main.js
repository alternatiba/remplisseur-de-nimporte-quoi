import "./global.css";
import App from "./App.svelte";

// Browser update notification https://browser-update.org/
import browserUpdate from "browser-update";
browserUpdate({
  required: {e: -4, f: -3, o: -3, s: -1, c: -3},
  notify_esr: true,
  text: {
    msg: "Ouh la la ! Ton navigateur ({brow_name}) n'est plus tout jeune !",
    msgmore:
      "Pour installer les dernières mises à jour de sécurité et pour éviter des difficultés d'affichage ou d'impression, tu devrais mettre ton navigateur à jour.",
    bupdate: "Ok, je mets à jour !",
    bignore: "Ignorer",
    remind: "On te le rappellera dans {days} jours.",
    bnever: "Ne me montre plus jamais ce message",
  },
});

const app = new App({target: document.body});

export default app;
